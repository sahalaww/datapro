<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Equipment;
use App\Unit;
use App\Measurement;

class ViewDataController extends Controller
{
    public function showMeasurment()
    {
        $measurement = Measurement::all();
        return Response::json($measurement);
    }
    public function showMeasurmentByUnit($id)
    {
        $measurement = Unit::find($id)->measurements()->get();
        return Response::json($measurement);
    }
    public function index(){
       return view('index'); 
    }
}
