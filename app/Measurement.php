<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Equipment;
use App\Unit;

class Measurement extends Model
{
    public function equipment()
    {
        return $this->belongsTo(Equipment::class,'equipment_id');
    }
    public function unit(){
        return $this->belongsTo(Unit::class,'unit_id');
    }
}
