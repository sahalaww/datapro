<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Unit;
use App\Measurements;
class Equipment extends Model
{
    protected $table = 'equipments';

    public function measurements()
    {
        return $this->hasMany(Measurement::class,'equipment_id');
    }
    
}
