<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\Measurement;

class Unit extends Model
{
    public function measurements()
    {
        return $this->hasMany(Measurement::class,'unit_id');
    }
}
