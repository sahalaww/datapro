<html>
    <head>
        <title>testing</title>
    </head>
    <body>
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Url</th>
            <th scope="col">Parameter</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Show All Data</td>
            <td><a href="{{ route('showAllMeasurment') }}">{{ route('showAllMeasurment') }}</a></td>
            <td>None</td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>Show Data by Unit Id</td>
            <td><a href="{{ route('showMeasurmentByUnit',1) }}" >/getDataUnit/$id</a></td>
            <td>$id</td>
            </tr>
            
        </tbody>
        </table>
    </body>
</html>
