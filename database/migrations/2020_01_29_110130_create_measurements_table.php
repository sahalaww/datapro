<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->bigInteger('unit_id')->unsigned()->index(); 
            $table->unsignedInteger('unit_id')->nullable();
            $table->unsignedInteger('equipment_id')->unsigned()->nullable();
            $table->float('moh')->default(0);
            $table->float('mop')->default(0);
            $table->float('mov')->default(0);
            $table->float('moa')->default(0);
            $table->float('mih')->default(0);
            $table->float('mip')->default(0);
            $table->float('miv')->default(0);
            $table->float('mia')->default(0);
            $table->float('pih')->default(0);
            $table->float('pip')->default(0);
            $table->float('piv')->default(0);
            $table->float('pia')->default(0);
            $table->float('poh')->default(0);
            $table->float('pop')->default(0);
            $table->float('pov')->default(0);
            $table->float('poa')->default(0);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
